export default {
  // Uncomment to enable dark theme
  //dark: true,
  themes: {
    light: {
      primary: '#534395', //(violet)
      accent: '#311B92', //(violet foncé)
      secondary: '#9fc519', //vert
      tertiary:"#b3a1ff", //(violet clair)
      success: '#00D28C',
      info: '#534395',
      warning: '#FF641E',
      error: '#F03C0E'
    },
    dark: {
      primary: "#81F19A",
      secondary: "#023D7F",
      tertiary:"#b3a1ff",
      accent: "#82B1FF",
      error: "#FF5252",
      info: "#003a5d",
      success: "#4CAF50",
      warning: "#FFC107"
    }
  }
}