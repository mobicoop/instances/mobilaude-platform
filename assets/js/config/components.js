// BASE
import MHeader from '@clientComponents/base/MHeader'
import MFooter from '@clientComponents/base/MFooter'
import Home from '@clientComponents/home/Home'
import MDialog from '@clientComponents/utilities/MDialog'
import Assistive from '@clientComponents/assistiveDevices/Assistive'

export default {
  MHeader,
  MFooter,
  Home,
  MDialog,
  Assistive
}