# mobilaude-platform

![logo](logo.png)

_DO NOT FORGET TO CHANGE THE APP_NAME INSIDE .env after creating client_
_FIND & REPLACE $$URL$$ & CHANGE IT TO THE URL OF THE DOMAINE WITHOUT dev/test/prod BUT LAB IF NEEDED_

mobilaude-platform is based on [mobicoop-platform](https://gitlab.com/mobicoop/mobicoop-platform)

_for developpement you have to use the same branch of mobicoop for eg (dev mobicoop with dev mobilaude-platform, master mobicoop=> master mobilaude-platform)_

# 🐳 With Docker 🐳

## 🐳 Requirements

Please check mobicoop [docker requirement](https://gitlab.com/mobicoop/mobicoop-platform/tree/dev/docs#-requirements-)

Clone & install via [docker way](https://gitlab.com/mobicoop/mobicoop-platform/tree/dev/docs#-install) the mobicoop platform

⚡️ Export env variable inside your .zshrc/.bashrc : `export MOBICOOP_CLIENT=/Full/Path/To/mobicoop-platform/client` _(client folder not the bundle!)_

## 🐳 Install

`make install`

## 🐳 Start

1. Start Mobicoop
2. Execute mobicoop fixtures
3. `make start`

you can now access mobilaude-platform from `localhost:9091`

## Infos

☣️ If you have to exclude files on mobicoop while developping, _DO NOT FORGET TO EXCLUDE THEM INTO THE [gitlab-exclude](./gitlab-exclude) FILE OF mobilaude-platform_ ☣️

_If you change any file into the mobilaude-platform mobicoop bundle, files are automatically changed in the mobicoop-platform folder too, so you will have to commit from the mobicoop-platform folder those changes_

#### Overwrite assets

If you want to overwrite a bundle assets in you client platform just recreate it inside `assets/` load the bundle one if you need the js & load you css

```javascript
import "@js/page/search/simpleResults.js";
import "@clientCss/page/search/simpleResults.scss";
```
